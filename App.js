/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Text, View } from 'react-native';
import { TextButton } from './app/ui_components/buttons';
import { Typography } from '@styles';

function App() {
  return (
    <View>
      <Text style={Typography.FONT_TITLE}> Welcome to React Native!</Text>
      <Text>To get started, edit App.js</Text>
      <Text>Wow</Text>
      <TextButton
        onPress={() => {
          console.log('clicked');
        }}
        label="Go head"
      />
    </View>
  );
}

export default App;
