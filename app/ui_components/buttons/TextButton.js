import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-unresolved
import { Buttons } from '@styles';

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#000',
    fontSize: 16,
    fontWeight: '600',
  },
  buttonStyle: {
    height: 45,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#38ba7d',
    borderBottomWidth: 6,
    borderBottomColor: '#1e6343',
    borderWidth: 1,
    marginLeft: 15,
    marginRight: 15,
  },
};

const TextButton = props => {
  const { textStyle } = styles;
  const { onPress, label } = props;
  return (
    <TouchableOpacity onPress={onPress} style={Buttons.base}>
      <Text style={textStyle}>{label}</Text>
    </TouchableOpacity>
  );
};

TextButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
};

export default TextButton;
