import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
  },
  buttonStyle: {
    height: 45,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#38ba7d',
    borderBottomWidth: 6,
    borderBottomColor: '#1e6343',
    borderWidth: 1,
    marginLeft: 15,
    marginRight: 15,
  },
};

const ImageButton = props => {
  const { buttonStyle, textStyle } = styles;
  const { onPress, source } = props;
  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Image style={textStyle} source={source} />
    </TouchableOpacity>
  );
};

ImageButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  source: Image.propTypes.source.isRequired,
};

export default ImageButton;
