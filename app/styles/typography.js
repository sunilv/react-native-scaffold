// Page titles, modal titles, Paragrah texts and links, inpits
export const FONT_TITLE = {
  fontSize: 17,
};

// Secondary text
export const FONT_SUBTITLE = {
  fontSize: 15,
};

// Tertiary text, Captions, Segmented, buttons
export const FONT_TERTIARY = {
  fontSize: 13,
};

// Action Bar
export const FONT_SMALL = {
  fontSize: 10,
};
