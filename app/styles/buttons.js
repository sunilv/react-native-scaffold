// eslint-disable-next-line import/no-extraneous-dependencies
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const base = {
  width: wp('20%'),
  height: hp('10%'),
};

export const small = {
  paddingHorizontal: 10,
  paddingVertical: 12,
  width: 75,
};

export const rounded = {
  borderRadius: 50,
};

export const smallRounded = {
  ...base,
  ...small,
  ...rounded,
};
